import 'package:fluro/fluro.dart';
import 'package:mm/views/login/login_page.dart';
import 'package:mm/views/login/register_page.dart';


var homeHandler = Handler(
  handlerFunc: (context, parameters) {
  return LoginPage();
});
var registerHandler = Handler(
  handlerFunc: (context, parameters) {
    return RegisterPage();
  }
);