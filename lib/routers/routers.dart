import 'package:fluro/fluro.dart';
import 'package:mm/routers/router_handler.dart';
class Routes {
  static String root = "/";
  static String loginPage = '/loginpage';
  static String register = '/register';
  
  static void configureRoutes(FluroRouter router) {
    // List widgetDemoList = new WidgetDemoList().getDemos();
    router.define(loginPage, handler: homeHandler);
    router.define(register, handler: registerHandler);
  }
}