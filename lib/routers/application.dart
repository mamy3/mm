import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
enum ENV {
  PRODUCTION,
  DEV,
}
class Application {
  static ENV env = ENV.DEV;
  static FluroRouter router = FluroRouter();
  
  Map<String, String> get config {
    if (Application.env == ENV.PRODUCTION) {
      return {};
    }
    if (Application.env == ENV.DEV) {
      return {};
    }
    return {};
  }

  static Future navigateTo(BuildContext context,String path) {
    return router.navigateTo(context, path,transition: TransitionType.inFromRight);
  }

  static Future navigateToParams(BuildContext context,String path,{required Map<String,dynamic> params}) {
    String query = "";
    if (params != null) {
      int index = 0;
      for (var key in params.keys) {
        var value = Uri.encodeComponent(params[params.keys]);
        if (index == 0) {
          query = "?";
        } else {
          query = query + "\&";
        }
        query += "$key=$value";
        index ++;
      }
    }
    path = path + query;
    return router.navigateTo(context, path, transition: TransitionType.inFromRight);
  }

  
}