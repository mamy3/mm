import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:mm/config/HttpUrlService.dart';
import 'package:mm/common/SharedPreferencesUtil.dart';


class HttpUtil {
  static late HttpUtil instance;
  late Dio dio;
  late BaseOptions options;
  static int a = 1;

  CancelToken cancelToken = CancelToken();

  static HttpUtil getInstance() {
    if (1 == a) {
      a = 2;
      instance = HttpUtil();
    }
    return instance;
  }

  dynamic requestInterceptor(RequestOptions options,RequestInterceptorHandler handler) async {
    return handler.next(options); //continue
  }
  dynamic responseInterceptor(Response response,ResponseInterceptorHandler handler) async {
    return handler.next(response); // continue
  }
  dynamic errorInterceptor(DioError dioError,ErrorInterceptorHandler handler) async {
    return  handler.next(dioError);//continue
  }
  /*
   * config it and create
   */
  HttpUtil() {
    // BaseOptions、Options、RequestOptions 
    // 都可以配置参数，优先级别依次递增，且可以根据优先级别覆盖参数
    options = BaseOptions(
      //请求基地址,可以包含子路径
      baseUrl: HttpUrlService.baseUrl,
      //连接服务器超时时间，单位是毫秒.
      connectTimeout: 10000,
      //响应流上前后两次接受到数据的间隔，单位为毫秒。
      receiveTimeout: 5000,
    );

    dio = Dio(options);

    // Cookie管理
    dio.interceptors.add(CookieManager(CookieJar()));

    // //添加拦截器
    dio.interceptors
        .add(InterceptorsWrapper(
      onRequest: (RequestOptions options,RequestInterceptorHandler handler) => requestInterceptor(options, handler),
      onResponse: (Response response,ResponseInterceptorHandler handler) => responseInterceptor(response,handler),
      onError: (DioError dioError,ErrorInterceptorHandler handler) => errorInterceptor(dioError,handler)
      ));
  }

  /*
   * get请求
   */
  get(url, {data, options, cancelToken}) async {
    var response;
    try {
      response = await dio.get(url,
          queryParameters: data, options: options, cancelToken: cancelToken);
      print('get success---------${response.statusCode}');
      print('get success---------${response.data}');

//      response.data; 响应体
//      response.headers; 响应头
//      response.request; 请求体
//      response.statusCode; 状态码

    } on DioError catch (e) {
      print('get error---------$e');
      formatError(e);
    }
    return response;
  }

  /*
   * post请求
   */
  post(url, {data, options, cancelToken}) async {
    late Response response;
    try {
      response = await dio.post(url,data: data);
      print('post success---------${response.data}');
    } on DioError catch (e) {
      print('post error---------');
      // formatError(e);
    }
    return response;
  }

  /*
   * error统一处理
   */
  void formatError(DioError e) {
    if (e.type == DioErrorType.connectTimeout) {
      // It occurs when url is opened timeout.
      print("连接超时");
    } else if (e.type == DioErrorType.sendTimeout) {
      // It occurs when url is sent timeout.
      print("请求超时");
    } else if (e.type == DioErrorType.receiveTimeout) {
      //It occurs when receiving timeout
      print("响应超时");
    } else if (e.type == DioErrorType.response) {
      // When the server response, but with a incorrect status, such as 404, 503...
      print("出现异常");
    } else if (e.type == DioErrorType.cancel) {
      // When the request is cancelled, dio will throw a error with this type.
      print("请求取消");
    } else {
      //DEFAULT Default error type, Some other Error. In this case, you can read the DioError.error if it is not null.
      print("未知错误");
    }
  }

  /*
   * 取消请求
   *
   * 同一个cancel token 可以用于多个请求，当一个cancel token取消时，所有使用该cancel token的请求都会被取消。
   * 所以参数可选
   */
  void cancelRequests(CancelToken token) {
    token.cancel("cancelled");
  }
}