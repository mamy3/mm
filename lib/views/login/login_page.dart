import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mm/routers/application.dart';
import 'package:mm/routers/routers.dart';
import 'package:mm/util/httpUtil.dart';
import 'package:mm/config/HttpUrlService.dart';
import 'package:dio/dio.dart';
import 'dart:convert';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}
class _LoginPageState extends State<LoginPage>{

  FocusNode _userNameFocusNode = new FocusNode();
  FocusNode _passwordFocusNode = new FocusNode();

  TextEditingController _userIdController= new TextEditingController();
  TextEditingController _passwordController= new TextEditingController();
  //表单状态
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  var _userName = '';
  var _passWord = '';
  var _isShowPwd = false;//是否显示密码
  var _isShowClear = false;//是否显示输入框尾部的清除按钮

  /**
   * 验证用户名
   */
  String validateUserName(value){
    // 正则匹配手机号
    RegExp exp = RegExp(r'^((13[0-9])|(14[0-9])|(15[0-9])|(16[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\d{8}$');
    if (value.isEmpty) {
      return '用户名不能为空!';
    }else if (!exp.hasMatch(value)) {
      return '请输入正确手机号';
    }
    return "";
  }

  /**
   * 验证密码
   */
  String validatePassWord(value){
    if (value.isEmpty) {
      return '密码不能为空';
    }else if(value.trim().length<6 || value.trim().length>18){
      return '密码长度不正确';
    }
    return "";
  }
  
  @override
  void initState() {
    super.initState();
  }

  login () async {
    Response respose;
    var data = {
      "USER_ID": _userIdController.text,
      "PASSWORD": _passwordController.text,
      "PHONE":"123"
    };
    respose = await HttpUtil.getInstance().post(HttpUrlService.loginUrl, data: data);
    if (respose.statusCode == 200) {
      var data= jsonDecode(respose.toString());//3
      if (data['ok']) {

      }
    }
  }

  @override
  Widget build(BuildContext context){
    ScreenUtil.init(
        BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: MediaQuery.of(context).size.height),
        designSize: Size(375, 812),
        orientation: Orientation.portrait);
    // 登录按钮区域
    Widget loginButtonArea = new Container(
      margin: EdgeInsets.only(left: 20,right: 20),
      height: 25.0,
      child: new RaisedButton(
        color: Colors.blue[300],
        child: Text(
          "登录",
        ),
        // 设置按钮圆角
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        onPressed: (){
          //点击登录按钮，解除焦点，回收键盘
          _userNameFocusNode.unfocus();
          _passwordFocusNode.unfocus();
          login();
          // if (_formKey.currentState.validate()) {
          //   //只有输入通过验证，才会执行这里
          //   //_formKey.currentState.save();
          //   //todo 登录操作
          //   print("$_userName + $_passWord");
          // }

        },
      ),
    );
    Widget InputTextArea = new Container(
      margin: EdgeInsets.only(left: 20,right: 20),
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: Colors.white
      ),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextFormField(
              controller: _userIdController,
              focusNode: _userNameFocusNode,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: '用户名',
                hintText: '请输入手机号',
                prefixIcon: Icon(Icons.person),
                suffixIcon: (_isShowClear)?IconButton(
                  icon: Icon(Icons.clear),
                  onPressed: (){
                    _userIdController.clear();
                  },
                ):null
              ),
              validator: validateUserName
            ),
            new TextFormField(
              controller: _passwordController,
              focusNode: _passwordFocusNode,
              decoration: InputDecoration(
                labelText: "密码",
                hintText: "请输入密码",
                prefixIcon: Icon(Icons.lock),
                // 是否显示密码
                suffixIcon: IconButton(
                  icon: Icon((_isShowPwd) ? Icons.visibility : Icons.visibility_off),
                  // 点击改变显示或隐藏密码
                  onPressed: (){
                    setState(() {
                      _isShowPwd = !_isShowPwd;
                    });
                  },
                )
              ),
              obscureText: !_isShowPwd,
              //密码验证
              validator: validatePassWord
            )
          ],
        ),
      ),
    );
    //忘记密码  立即注册
    Widget bottomArea = new Container(
      margin: EdgeInsets.only(right: 20,left: 30),
      child: new Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FlatButton(
        child: Text(
          "忘记密码?",
          style: TextStyle(
          color: Colors.blue[400],
          fontSize: 16.0,
          ),
        ),
        //忘记密码按钮，点击执行事件
        onPressed: (){

        },
        ),
        FlatButton(
        child: Text(
          "快速注册",
          style: TextStyle(
          color: Colors.blue[400],
          fontSize: 16.0,
          ),  
        ),
        //点击快速注册、执行事件
        onPressed: (){
          Application.navigateTo(context, Routes.register);
        },
        )
      ],
      ),
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: new GestureDetector(
        onTap: (){
          _userNameFocusNode.unfocus();
          _passwordFocusNode.unfocus();
        },
        child: ListView(
          children: <Widget>[
            new SizedBox(height: 80),
            InputTextArea,
            SizedBox(height: 80),
            loginButtonArea,
            SizedBox(height: 80),
            bottomArea
          ],
        ),
      ),
    );
  }
}