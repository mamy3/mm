
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:mm/config/HttpUrlService.dart';
import 'package:mm/util/httpUtil.dart';
import 'dart:convert';
import 'package:mm/routers/application.dart';
import 'package:mm/routers/routers.dart';


class RegisterPage extends StatefulWidget {
  @override
  createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  // ignore: prefer_typing_uninitialized_variables
  var _userNameController;
  var _nickNameController;
  // ignore: prefer_typing_uninitialized_variables
  var _passWordController;
  // ignore: prefer_typing_uninitialized_variables
  var _phoneController;
  var _areaCodeController;
  var _addrController;
  
  Padding buildTextField(String title, 
  TextEditingController controller, String helperText, 
  String hintText, Icon? icon) {
    return Padding(
      padding: EdgeInsets.fromLTRB(40, 0, 40, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text(title , textAlign: TextAlign.left)
          ),
          Expanded(
            flex: 4,
            child: TextFormField(
              controller: controller,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.done,
              maxLength: 20,
              decoration: InputDecoration(
                helperText: helperText,
                helperStyle: TextStyle(
                  color: Colors.blue
                ),
                hintText: hintText,
                // icon: icon,
                prefixIcon: icon,
                suffixIcon: IconButton(
                  onPressed: (){
                    _userNameController.clear();
                  }, 
                  icon: Icon(Icons.close)
                )
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _userNameController = new TextEditingController();
    _nickNameController = new TextEditingController();
    _passWordController = new TextEditingController();
    _phoneController = new TextEditingController();
    _addrController = new TextEditingController();
    _areaCodeController = new TextEditingController();
  }
  void showCupertinoAlertDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text("成功！"),
            content: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("确定"),
                onPressed: () {
                  Application.navigateTo(context, Routes.loginPage);
                },
              )
            ],
          );
        });
  }
  register () async {
    Response respose;
    var data = {
      "USER_NAME": _userNameController.text,
      "PHONE": _phoneController.text,
      "ADDR": _addrController.text,
      "PASSWORD": _passWordController.text,
      "NICK_NAME": _nickNameController.text,
      "AREA_CODE": _areaCodeController.text,
    };
    HttpUtil instance = HttpUtil.getInstance();
    respose = await instance.post(HttpUrlService.registerUrl, data: data);
    if (respose.statusCode == 200) {
      var data= jsonDecode(respose.toString());//3
      if (data['ok']) {
        showCupertinoAlertDialog();
      }
    }
  }
  @override
  Widget build (BuildContext context){
    return Scaffold(
      body:Container(
      // decoration: BoxDecoration(
      //   image: DecorationImage(
      //     image: ExactAssetImage('images/beijing.jpg'),
      //     fit: BoxFit.cover,
      //   )
      // ),
      child: Column(
        children: <Widget>[
          buildTextField('真实姓名', _userNameController, '不支持特殊字符', '请输入真实姓名', Icon(Icons.person)),
          buildTextField('昵称', _nickNameController, '不支持特殊字符', '请输入昵称' , Icon(Icons.phone)),
          buildTextField('手机号', _phoneController, '将作为登录账户使用', '请输入手机号', Icon(Icons.phone)),
          buildTextField('密码', _passWordController, '不支持特殊字符', '请输入密码', Icon(Icons.lock)),
          buildTextField('区号', _areaCodeController, '将作为登录账户使用', '请输入区号', Icon(Icons.phone)),
          buildTextField('地址', _addrController, '将作为登录账户使用', '请输入地址', Icon(Icons.phone)),
          MaterialButton(
            color: Colors.white,
            textColor: Colors.grey,
            child: Text('注册'),
            onPressed: () => register()
          )
        ],
      ),
    ));
  }
}